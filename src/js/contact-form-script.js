$("#contactForm").validator().on("submit", function (event) {
    if (event.isDefaultPrevented()) {
        // handle the invalid form...
        formError();
        submitMSG(false, "Você precisa preencher o formulário corretamente");
    } else {
        // everything looks good!
        event.preventDefault();
        submitForm();
    }
});


function submitForm(){
    // Initiate Variables With Form Content
    var nome =     $("#nome").val();
    var email =    $("#email").val();
    var tel =      $("#tel").val();
    var wts =      $("#wts").val();
    var assunto =  $("#assunto").val();
    var mensagem = $("#mensagem").val();
    


    $.ajax({
        type: "POST",
        url: "send_mail.php",
        data: "&nome=" + nome + "&email=" + email + "&tel=" + tel + "&wts=" + wts + "&assunto=" + assunto + "&mensagem=" + mensagem,
        success : function(text){
            if (text == "success"){
                formSuccess();
            } else {
                formError();
                submitMSG(false,text);
            }
        }
    });
}

function formSuccess(){
    $("#contactForm")[0].reset();
    submitMSG(true, "Mensagem enviada com sucesso!")
}

function formError(){
    $("#contactForm").removeClass().addClass('shake animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
        $(this).removeClass();
    });
}

function submitMSG(valid, msg){
    if(valid){
        var msgClasses = "tada animated text-success";
    } else {
        var msgClasses = "text-danger";
    }
    $("#msgSubmit").removeClass().addClass(msgClasses).text(msg);
}