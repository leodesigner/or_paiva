//slick menu
$(window).on('scroll',function() {
  var scrolltop = $(this).scrollTop();

  if(scrolltop >= 180) {
    $('.header-default').addClass('fixed-top-or');
  }

  else if(scrolltop <= 180) {
    $('.header-default').removeClass('fixed-top-or');
  }

  if(scrolltop >= 1200) {
      $(".animated").addClass("fadeInUp");
  } 

});

$(document).ready(function(){
  //owCarousel
  $('.owl-carousel').owlCarousel({
    margin: 10,
    responsive:{
        0:{
            items:1.5,
            stagePadding: 10,
        },
        600:{
            items:3
        },
        1000:{
            items:2.5
        }
    }
  });

  //Mask
  $('.mask').mask('(00) 00000.0000');  
});